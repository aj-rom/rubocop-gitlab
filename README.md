# RuboCop GitLab

This is a CI template to utilize [RuboCop](https://docs.rubocop.org/) in GitLab Pipelines.

## Usage

Simply [include](https://docs.gitlab.com/ee/ci/yaml/includes.html) the component into your own CI configuration.

```yaml
# .gitlab-ci.yml

include:
  - component: 'gitlab.com/aj-rom/rubocop-gitlab@v1.0'
  # or
  - remote: 'https://gitlab.com/aj-rom/rubocop-gitlab/-/raw/v1.0/template.yml'
  # or
  - project: 'aj-rom/rubocop-gitlab'
    ref: 'v1.0'
    file: 'template.yml'

```

Or with inputs:

```yaml
include:
  - component: 'gitlab.com/aj-rom/rubocop-gitlab@v1.0'
    inputs:
      job-name: 'rubocop' # name of the job being defined with the following inputs
      job-stage: 'test' # stage for the job to run
      job-tag: 'ruby' # tags for the runner
      image: 'ruby:latest' # specify your docker image here
      work-dir: './' # Gemfile location
      bundle-path: 'vendor/bundle' # bundle install directory
      bundle-gemfile: 'Gemfile' # Gemfile to use for dependency resolution
      bundle-quiet: true # whether or not install gems quietly
      rubocop-options: '-E -P' # rubocop execution options
      rubocop-pattern: '.' # rubocop execution pattern
```

### Available Inputs

| Key | Description | Default |
| --- | --- | --- |
| `image` | The docker image to use for the job. | `ruby:latest` |
| `job-name` | Unique name of the job, used for defining multiple of the same job. | `rubocop` |
| `job-stage` | The stage for the job to run in. | `test` |
| `work-dir` | The directory to run `bundle install` and `rubocop` in. | `./` |
| `bundle-path` | Where to install ruby gems to, this input is expanded to the `BUNDLE_PATH` variable for install. | `vendor/bundle` |
| `bundle-gemfile` | The `Gemfile` to use for dependency resolution, this input is expanded to the `BUNDLE_GEMFILE` variable for install. | `Gemfile` |
| `rubocop-options` | Arguments to pass to `bundle exec rubocop <ARGUMENTS>`, this input is expanded to the `RUBOCOP_OPTIONS` variable for runtime. | `-E -P` (-E, extra details, -P, utilize all available CPUs) |
| `rubocop-pattern` | The rubocop execution file glob pattern, this input is expanded to the `RUBOCOP_PATTERN` variable. | `.` (All files relative to `work_dir`) |

## Job Caching

The `rubocop` job is cached on a per-job basis keyed with the `$[[ inputs.work-dir ]]$[[ inputs.bundle-gemfile ]].lock` file hash.

This way we can have the job cache updated if our dependency lockfile ever changes.

> If your project does not use a `Gemfile.lock`, caching would not apply, as the dependency tree would need to be checked every install.

## [Examples](https://gitlab.com/aj-rom/rubocop-gitlab/-/tree/main/examples?ref_type=heads)

- [default](https://gitlab.com/aj-rom/rubocop-gitlab/-/tree/main/examples/default?ref_type=heads)
- [optional-group](https://gitlab.com/aj-rom/rubocop-gitlab/-/tree/main/examples/optional-group?ref_type=heads)